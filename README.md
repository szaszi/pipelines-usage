## Bitbucket Pipelines usage calculator

This is a NodeJS script that will calculate your Bitbucket Pipelines build minutes using the Bitbucket REST API.

Requirements:

* node 7.10 or later

### Installing dependencies

Before you can run the script, you need to install dependencies with npm:

`npm install`

### Running the script

To use the script, set `BB_PASSWORD` environment variable to your Bitbucket password (or create an "App password" via the Bitbucket settings), then pass your username and repositories to query on the command line.

```
export BB_PASSWORD=secret
node pipelines-usage.js USERNAME REPO_SLUG ...
```

Repositories can be specified by a slug relative to your account (`some_repo`), or qualified with an account name (`some_account/some_repo`).

### Sample output

Here's a sample run of the tool:

```
$ node pipelines-usage.js mryall_atlassian pipelines-test bitbucketci/bitbucket-ui atlassian/atlaskit
Showing builds in the last 30 days
Repository  Builds  Build minutes
pipelines-test  56  6m 35s
bitbucketci/bitbucket-ui    161 9h 6m 17s
atlassian/atlaskit  2177    1220h 0m 20s
```

The values in the output are tab separated.

Note that results come back asynchronously (each repository is processed in parallel), so the order of repositories in the output will vary.

